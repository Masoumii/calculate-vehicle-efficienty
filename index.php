<?php

if(isset($_POST["submit"])){
  
   $postName = $_POST['name'];
   $postType = $_POST['type'];
   $postSpeed = $_POST['speed'];
   $postPassenger = $_POST['passenger'];
   $postCost = $_POST['cost'];
  
require_once('class.vehicle.php');
$vehicle = new Car();
$vehicle->setName($postName);
$vehicle->setType($postType);
$vehicle->setMaxSpeed($postSpeed);
$vehicle->setMaxPassengers($postPassenger);
$vehicle->setCosts($postCost);

  echo $vehicle->getName().": ".$vehicle->checkEfficiency()."</br>";
}

?>

    <form method="POST" action="">
      <input type="text" name="name" placeholder="Name" required></br>
      <input type="text" name="type" placeholder="Type of transport" required></br>
      <input type="text" name="speed" placeholder="Max speed in Kilometers" required></br>
      <input type="text" name="passenger" placeholder="Max amount of passengers" required></br>
      <input type="text" name="cost" placeholder="Costs per Kilometers" required></br>
      
      <input type="submit" name="submit" value="Check efficiency">
    </form>
