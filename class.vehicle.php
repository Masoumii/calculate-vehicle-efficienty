<?php

// Superclass
abstract class Vehicle {
  
  protected $name = null;
  protected $type = null;
  protected $maxSpeed = null;
  protected $maxPassengers = null;
  protected $costsPerKm = null;
  protected $points = null;
  protected $stars = null;

  
  public function __construct(){}



  public function getName(){
   return $this->name;
  }
  
  public function setName($name){
   $this->name = $name;
  }
  
  public function getType(){
    return $this->type;
  }
  
  public function setType($type){
    $this->type = $type;
  }
  
  public function getMaxSpeed(){
    return $this->maxSpeed;
  }
  
  public function setMaxSpeed($maxSpeed){
    $this->maxSpeed = $maxSpeed;
  }
  
  public function getMaxPassengers(){
    return $this->$maxPassengers;
  }
  
  public function setMaxPassengers($maxPassengers){
    $this->maxPassengers = $maxPassengers;
  }
  
  public function getCosts(){
    return $this->costsPerKm;
  }
  
  public function setCosts($costsPerKm){
    $this->costsPerKm = $costsPerKm;
  }
  
  
  public function checkEfficiency(){
    
    
    // Gives points according to costs per kilometers
      if($this->costsPerKm < 0.50){
        $this->points += 25;
      }
    
      if($this->costsPerKm > 0.50 && $this->costsPerKm < 1.50){
        $this->points += 17;
      }
    
      if($this->costsPerKm >= 1.50 && $this->costsPerKm < 2){
        $this->points += 11;
      }
    
      if($this->costsPerKm >= 2){
        $this->points += 5;
      }
    
    
    // Gives points according to max-speed
    if($this->maxSpeed > 80 && $this->maxSpeed < 150){
        $this->points += 8;
      }
    
     if($this->maxSpeed >= 150 && $this->maxSpeed < 250){
        $this->points += 10;
      }
    
    if($this->maxSpeed >= 250 && $this->maxSpeed <350){
        $this->points += 13;
      }
    
      if($this->maxSpeed >= 350 && $this->maxSpeed < 450){
        $this->points += 17;
      }      
    
      if($this->maxSpeed >= 450){
        $this->points += 25;
      }
    
    
    // Gives points according to transportation type
     if($this->type === 'Air'){
        $this->points += 25;
      }
    
      if($this->type === 'Sea'){
        $this->points += 15;
      }
    
      if($this->type === 'Land'){
        $this->points += 8;
      }
    
    
    if($this->maxPassengers === 1){
      $this->points +=1;
    }
    
     if($this->maxPassengers > 1 && $this->maxPassengers < 3){
      $this->points +=7;
    }
    
      if($this->maxPassengers >= 3 && $this->maxPassengers < 5){
      $this->points +=13;
    }
    
        if($this->maxPassengers > 5 ){
      $this->points +=25;
    }
    
     if($this->points <= 20){
       $this->stars = "<img src='img/star.png'>";
     }
    if($this->points > 20 && $this->points <= 40){
       $this->stars = "<img src='img/star2.png'>";
     }
     if($this->points > 40 && $this->points <= 60){
       $this->stars = "<img src='img/star3.png'>";
     }
    if($this->points > 60 && $this->points <= 80){
       $this->stars = "<img src='img/star4.png'>";
     }
    if($this->points > 80 && $this->points <= 100){
       $this->stars = "<img src='img/star5.png'>";
     }
    
    
    return $this->points."% - ".$this->stars;
  }
  
}

// sub-class
class Airplane extends Vehicle{
  public function __construct(){}
}

// sub-class
class Car extends Vehicle{   
  public function __construct(){}
}

// sub-class
class Boat extends Vehicle{
  public function __construct(){}
}

// sub-class
class Bicycle extends Vehicle{
  public function __construct(){}
}

?>